package co.jessicajoseph.meowa;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by Jess on 8/9/15.
 */
public class MainActivity extends Activity{

    private Button enterButton;

    private ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.introduction);

        mImageView = (ImageView) findViewById(R.id.image_view);
        mImageView.setImageResource(R.drawable.meyow_or);

        enterButton = (Button) findViewById(R.id.enter);

        enterButton.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            Intent mapsIntent = new Intent(view.getContext(),MapsActivity.class);

            startActivity(mapsIntent);
        }
    });


    }
//    @Override
//    public View onCreateView(
//            LayoutInflater inflater,
//            ViewGroup container,
//            Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.splash, container, false);
//
//        loginButton = (LoginButton) view.findViewById(R.id.login_button);
//        loginButton.setReadPermissions("user_friends");
//        // If using in a fragment
//        loginButton.setFragment(this);
//        // Other app specific specialization
//
//        // Callback registration
//        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                // App code
//            }
//
//            @Override
//            public void onCancel() {
//                // App code
//            }
//
//            @Override
//            public void onError(FacebookException exception) {
//                // App code
//            }
//        });
//    }


}
